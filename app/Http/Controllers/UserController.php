<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;

use App\Repositories\UserRepository;

class UserController extends BaseController
{

    private UserRepository $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function index() {
        $test = $this->user->all();
        dd($test);
    }
}