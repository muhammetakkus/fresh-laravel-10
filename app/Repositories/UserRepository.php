<?php
namespace App\Repositories;
use App\Repositories\IUserRepository;
use App\Models\User;

class UserRepository implements IUserRepository {
    public function all() {
        $users = User::all();
        return $users;
    }
}