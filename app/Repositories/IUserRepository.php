<?php

namespace App\Repositories;

interface IUserRepository
{
    public function getUsers();
}
